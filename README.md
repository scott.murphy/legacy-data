# October Talk for ovSAGE/LOPSA Ottawa

Testing this out as a method of making the slides available, as well as keeping
things in a git repo. The impetus was a recent talk that mentioned gitpitch and
the pitchme.md file. So far I like it.

There is one thing to remember when loding the URL, if it is not on github, you
need to tell it what git system you are using. This one is on gitlab, so
either select it from the menu on the left OR add `?grs=gitlab` to the URL. You
can also specify the theme by adding a &t=<theme>.

