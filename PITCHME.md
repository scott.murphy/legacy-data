---?image=assets/slide_bg.png&size=auto 90%

## Legacy Data Recovery

"Fun" with old backups

#HSLIDE

## What motivated me?

The short answer is that I ran across a box of tapes while looking for
something else and decided that I should see if they were readable. The
result was a journey down memory lane and a quest for various pieces
of hardware to make this work.

#HSLIDE

## First things

What did I find?

A bunch of QIC tapes. The QIC means Quarter Inch Cartridge and they
came in a wide variety of capacities. The ones I had were QIC-60 or 
QIC-150 tapes.

#HSLIDE

![Data Cartridge](assets/qictape.png)

#HSLIDE

## QIC Design

- Encased 5.25" reel-to-reel tape
- Tension band to keep the tape from unspooling and to act as a drive mechanism
- Drive capstan to move the tape forward and backward
- Movable door to protect the tape

#VSLIDE

![Data Cartridge Mechanism](assets/qic_mechanism.png)

#VSLIDE

## Is recovery likely?

They are known to retain data for an extended time frame. In this case they are
roughly 25 years old. They have been in a fairly controlled environment for at
least 20 of those years, so they might just retain their data

#VSLIDE

## Known Issues

- Sticky Shed Syndrome
- Loose tension band
- Degraded drive capstan

#VSLIDE

## Sticky Shed Syndrome

The polyester medium, adhesive, and metal oxide start to break down a little and
the tape starts sticking to itself and transferring the magnetix particles to
the wrong side of the tape. In bad cases, the mylar film loses all the coating
and becomes transparent, causing other issues

#VSLIDE

## Loose tension band

If the band has stretched during all of this time, it will no longer maintain a
strong contact to the actual tape surface and will pop off during operation,
possibly pulling tape with it and despooling , stretching, or damaging the tape
in a number of intersting ways. I have many real examples.

#VSLIDE

## Degraded drive capstan

The capstan has developed a flat spot due to it being left in a drive for a
long period of time. As a result, the matching wheel hits the flat spot and can
no longer make the tape stream forward or backward

#HSLIDE

## My Tapes

In my case, I had a wheel melt. Fun!

#VSLIDE

The tape was stuck and as a result, it wouldn't move when the motor was
turning. The tension band does not replace a direct drive capstan system like
an old casette tape.

#VSLIDE

I noticed it was taking a very long time for the tape to retension.  I ended up
ejecting the tape while running, as there was something wrong and the software
would not abort. I could have powered down the computer, but that might have
been worse. This was an old computer running an older version of Linux.

#VSLIDE

Luckily, I was retensioning rather than trying to read the tape. As I said, 
I didn't notice it for a while. I thought it was working properly and
as the motor spun, the wheel edge closest to the motor got warm from friction
and started to melt. It did a very good job.

#VSLIDE

When the tape was ejected, the capstan was covered with a black sludge. At this
point, I had not realized that the wheel had melted, I wondered what had
possibly leaked into the tape drive. It was black, sticky, and left black
residue on anything it touched.

#VSLIDE

Oddly enough, it barely touched the actual tape, as the tension band let go and
the tape was not moving. I have transplanted the tape into another cartridge,
but have not tried reading it yet. The tension band was a little loose and
during retension, it slipped off. I have to fix the band before I can use it.

#HSLIDE

## Trifecta!

As you can tell by the little story, I have hit the trifecta of issues on a
single tape. There are a number of ways to go about resurrecting the tape and
recovering the data.

#VSLIDE

## Fix #1

Baking the tape:

- Take the plastic housing off
- Remove the tension band and drive capstan
- Loosen the tape a little
- Place the tape in a hot (58°C) oven for 24 hours

#VSLIDE

## Fix #2

Boiling the tension band:

- Put the band in a cup
- Add some boiling water from a kettle
- Clean the belt
- Place it back in the cartridge

#VSLIDE

## Bitsavers.org message

    On 8/27/14 2:17 AM, Christian Corti wrote:

    > But you can "restore" a worn our belt by applying heat. The best is to pour hot \
    > water (70°C should be good enough) and you'll see how the belt shrinks and regains \
    > its dark black color. After that, clean the belt (it may have some sticky residues \
    > on the surface) and restore it in the cartridge. The shrunk belt has a length of \
    > 12-18 cm instead. I've successfully restored many belts with that method.

#VSLIDE

## Fix #3

Deformed/Melted drive wheel

In this case, I used another cartridge case, as the other had too much gunk on
it to bother cleaning. I had one that had a old tape, so I just opened it and
replaced the tape. This is the one that the band slipped on. 

#VSLIDE

I have to do fix #2 next. I manually wound the tape back and forth to see how
much tape was stuck together and did not find any real issues. I was
retensioning it when the band slipped. I'll have a followup once I get it
tested.

#HSLIDE

## Working tapes

I did manage to find tapes with valid data on them and they retensioned
properly. I know they were in a older tape format, but I could still dump them,
just in case.

#VSLIDE

## Dumping a DOS Tape

As you might have noticed, this PC has Linux installed and the drive is a SCSI
drive. I did have to install the mt utilities, as they are not installed by
default.

![QIC-150 Drive](assets/qic-150_drive.png)

#VSLIDE

## Step 1

You know there is data, so you need to dump everything until you reach the end of the tape.

```
mt -f /dev/nst0 retension
dd if=/dev/nst0 of=part1 bs=512b
```

Repeat until you reach the end of tape, incrementing the output file name

#VSLIDE

## Discoveries

When I performed this, I ended up with several small  files which appear to be markers for the tape software and two large data files.

The later ones showed table of content entries for the large files

#VSLIDE

## Now what?

Well, the first thing that is obvious is that I can write these back to a tape if I need to, reversing the process.

The second thing is that if I can write back to a tape, I can probably combine
this into a file. Further investigation indicated that pretty much all
microsoft systems use the same format for tapes which was developed by Maynard
back in the early 80s.

#VSLIDE

## NTBackup

According to many sources, NTBackup uses this format, called MTF (Microsoft Tape Format) for all backup tapes and has been consistent across multiple generations. 

#VSLIDE

## Linux Software

Someone contributed an mtf library to the open source movement and another
person created a mtftar program that can createa tar file out of a ntbackup
file. Things are looking hopeful.

#VSLIDE

## Fail #1

It appears that the tape is not in NTBackup format. Oh well, back to the
drawing board.

#VSLIDE

## What else?

I have two QIC 5.25" tape drives from back in the day. One has a QIC-02 25 pin
interface that connects to a proprietary card mde by Archive Corp and the other
has a DB37 interface and a matching QIC-36 card I think. I'll have to resurrect
these and see what happens

#VSLIDE

## What else?

The QIC-36 style interface has power over the cable, so no external power
required. I tried that one first. The system comes up, but does not recognize
the drive. It has jumpers on the card, so that might be part of the issue.

Further research indicates that this is the more primitive interface and will
not operate in a system that is faster than 25MHz. My ISA slot system is a
300MHZ Pentium, so that might explain it

#VSLIDE

## What else?

Installing the QIC-02 interface and hooking up the drive seemed a little more
promising. Unfortunately, it was not recognized by my initial test.

I think It might be recognized by the backup program under windows95 or
windows98. At least I have seen references to it.

#VSLIDE

## What else?

I also found a single floppy disk (5.25") with an old version of Archive
QICstream software. That looks promising, as the QIC-02 interface is branded
Archive and so is the matching tape drive.

Time for DOS

#HSLIDE

## The DOS Way

I found a set of DOS 6.2 disketes and a working 3.5" drive in order to give it
a try.  I have not run anything like this in roughly 22 years, as I had Windows
95 on my PC pretty much the day of the product launch. I attended the launch
party in Ottawa and received a free copy, as well as a copy of Office.

#VSLIDE

## Rememebering Bill

Gates that is. Do you know how much fun it is to resurrect DOS on old hardware
and having to remember the limitations?  

#VSLIDE

## Legacy Hardware

To get this to work, I needed some pretty specific hardware, all of which I had
in abundance 15 years ago, but not so much now.

#VSLIDE

## Build List

- Motherboard with one, possible two ISA slots
- VGA card that works in such a beast. I managed to find an AGP card in my
  boxes of old cards
- 3.5" floppy drive
- 5.25" floppy drive
- 2GB or smaller HDD

#VSLIDE

## Inventory Fail

I had everything but the system with a working ISA slot. I asked around and 
borrowed a Pentium from a friend to see if the tape controller and unit still
worked. I added the 5.25" drive and the tape unit and to my surprise, they
came up and were recognized.

#VSLIDE

## Inventory Fail

Recognized being a term that states that I could see the hardware.

It does not mean I could use it immediately. I also had the existing system
disk running windows 95 to use as a base. Windows saw unconfigured hardware
which I would guess was the tape drive. Not usable at this point.

#VSLIDE

## Inventory Fail

I built a boot floppy for DOS 6.22 and successfully booted it. I also managed
to install the tape software from the 5.25" disk (which I have since imaged).
Running the software did not get me much, as it could not find the unit. There
was a configuration tool that told me what jumpers to set and I had to shutdown
to remove the card and set it, but after that it worked.

#VSLIDE

## Inventory Fail

At this point, I managed to load a tape, retension it and get a catalog. Things
are looking good now.

#VSLIDE

## Inventory Non-Fail

Since this was starting to look like I could proceed, I managed to find a
Pentium on eBay for next to nothing. It was pristime, like someone had put it
aside 20 years ago and wrapped it in plastic. It has 256MB RAM and 4 ISA slots.

Of course, it did not boot.

#VSLIDE

## Inventory Quasi-Fail

After poking around a bit, I noticed that the processor frame was loose. During
shipping the CPU had popped out but was held in place by the vertical frame
that some pentium chips have. Slotting it back in and pushing I heard a solid
click and now it powers up.

#VSLIDE

## Inventory Complete

now that I have a full system, I installed the 8GB HDD from 1999 with the
capacity jumper set to 2GB and booted disk 1 of DOS 6.22 installer. It took a
long time to get DOS installed on the system. I had forgotten about how long it
takes to format a hard drive on an old chassis.

#VSLIDE

## Installation of software

Once the system was in place, I installed the tape software from the 5.25"
floppy and managed to restore the backup to an alternate location. 

Success in reading a 25 year old backup

#VSLIDE

## Another Fail

I decided to restore the backup to the boot drive, but I ended up with a
slipped tension band, so I have to boil the band next. Good thing I am doing
this for fun and not profit, as I could have sent this off for professional
recovery and saved money if I was charging for the task.

#VSLIDE

## Next Steps

Since I have a copy of the two separate tapes from my initial test, I can
rebuild a disk with some command line magic and convert the whole thing to a
VM. I'm kind of fascinated, as this was my development environment for Clipper
and has windows 3.1, Autocad, Word, Word for Windows, and a plethora of other
stuff.

#VSLIDE

## Next Steps

In addition, it has a rather fancy configuration setup to reconfigure the
system and reboot into one of four environments for doing things. 

#VSLIDE

## Nostalgia

One of these is my Senior Wizard environment from my days on the DragonFire
LPMud back in the early 90s. I still have an account there and much of my
software is still running.

#HSLIDE

## Other Ideas

I have some CF to IDE adapter cards and some industrial grade CF cards in the 1
and 2GB range. I think I'll try to use these as HDD units and see what happens.
I'll do a followup after I have tried it out. You never know if any of this
will have value for some obsolete system that has somethign important you need
to access a quarter of a century later.

#HSLIDE

# MTFTAR Demo

#HSLIDE

## Emulations

- Windows98SE in VMWare
- MS-DOS in VMWare
- the OCLUG webserver in VMWare (that will be a future talk at OCLUG)

#HSLIDE

## References

- http://www.wpusa.dynip.com/gtakgtar/gtak/044_L1_BlocksandSizes.html
- http://qicreader.blogspot.ca/
- https://en.wikipedia.org/wiki/Maynard_Electronics
- https://en.wikipedia.org/wiki/Backup_Exec

#VSLIDE

- http://pnijjar.freeshell.org/2010/mtftar/
- http://www.savvyadmin.com/extract-nt-backup-files-in-linux-using-mtftar/
- https://www.108.bz/posts/it/recovering-ntbackup-tapes/
- http://www.qic.org/

#VSLIDE

- https://en.wikipedia.org/wiki/Quarter-inch_cartridge
- http://oldcomputer.info/media/faq/index.htm
- http://encyclopedia2.thefreedictionary.com/QIC
- http://www.series80.org/Articles/tape-considerations.html
- http://www.dreamlandbbs.com/drivers/tape/index.html

